package pl.gitlab.konrad92.crrs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrrsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrrsApplication.class, args);
	}

}
